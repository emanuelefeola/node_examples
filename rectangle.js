﻿module.exports = (x, y, callback) => {
    if (x <= 0 || y <= 0) {
        setTimeout(() =>
            callback(new Error("Incorrect dimensions"), null),
            2000);
    } else {
        setTimeout(() => callback(null, {
            perimeter: () => (2 * (x + y)),
            area: () => (x * y)
        }), 2000);
    }
}

/*
* la callback contiene il valore di ritorno.
* il primo parametro contiene l'eventuale errore
* il secondo parametro contiene un oggetto javascript che ha due proprietà (p, a)
*/
