﻿var rect = require('./rectangle');

function solveRect(l, b) {
    console.log("l = " + l + " and b = " + b);

    rect(l, b, (err, rectangle) => {
        if (err) {
            console.log("ERROR " + err.message);
        } else {
            console.log("Area: " + rectangle.area() + "\n");
            console.log("Perimeter: " + rectangle.perimeter() + "\n");
        }
    });

    console.log("rect() is called");


};


solveRect(2, 4);
solveRect(3, 5);
solveRect(0, 2);
solveRect(3, -4);